/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.wysiwyg.listeners;

import java.awt.event.ActionEvent;
import javax.swing.JEditorPane;
import javax.swing.text.MutableAttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.StyledEditorKit.StyledTextAction;

/**
 *
 * @author imyousuf
 */
public class HeadingAction extends StyledTextAction {
    
    public HeadingAction() {
        super("H");
    }

    public void actionPerformed(ActionEvent e) {
        JEditorPane editor = getEditor(e);
        System.out.println(editor);
        System.out.println(e.getSource());
        System.out.println(e.getSource().getClass());
        if (editor != null) {
            StyledEditorKit kit = getStyledEditorKit(editor);
            MutableAttributeSet attr = kit.getInputAttributes();
            boolean bold = (StyleConstants.isBold(attr)) ? false : true;
            SimpleAttributeSet sas = new SimpleAttributeSet();
            StyleConstants.setBold(sas, bold);
            boolean italic = (StyleConstants.isItalic(attr)) ? false : true;
            StyleConstants.setItalic(sas, italic);
            setCharacterAttributes(editor, sas, false);
        }
    }

}
