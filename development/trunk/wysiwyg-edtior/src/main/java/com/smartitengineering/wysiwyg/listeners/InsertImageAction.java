/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smartitengineering.wysiwyg.listeners;

import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.JEditorPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.StyledEditorKit.StyledTextAction;
import javax.swing.text.html.HTMLDocument;

/**
 *
 * @author imyousuf
 */
public class InsertImageAction extends StyledTextAction {
    
    protected JTextPane textPane;

    public InsertImageAction(JTextPane textPane) {
        super("insert-image");
        this.textPane = textPane;
    }

    public void actionPerformed(ActionEvent e) {
        JEditorPane editor = getEditor(e);
        System.out.println(editor);
        System.out.println(e.getSource());
        System.out.println(e.getSource().getClass());
        if(editor == null) {
            editor = textPane;
        }
        if (editor != null && textPane.getCaretPosition() > -1) {
            try {
                HTMLDocument document = (HTMLDocument) editor.getDocument();
                Element element = document.getCharacterElement(textPane.getCaretPosition());
                System.out.println(element.toString());
                document.insertAfterEnd(element, "<img src=\"file:///home/imyousuf/docs/images/OpenArticleInBrowser.png\" />");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        else {
            System.out.println("ELSE");
        }
        System.out.println(textPane.getText());
    }
}
