package com.smartitengineering.wysiwyg;

import com.smartitengineering.wysiwyg.editor.RichTextEditorUI;
import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        RichTextEditorUI editorUI = new RichTextEditorUI();
        JFrame frame = new JFrame();
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(editorUI, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);
    }
}
