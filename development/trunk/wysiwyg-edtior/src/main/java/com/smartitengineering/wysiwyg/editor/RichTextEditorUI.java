/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.smartitengineering.wysiwyg.editor;

import com.smartitengineering.wysiwyg.listeners.HeadingAction;
import com.smartitengineering.wysiwyg.listeners.InsertImageAction;
import java.awt.BorderLayout;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.JToolBar;
import javax.swing.text.StyledEditorKit;

/**
 *
 * @author imyousuf
 */
public class RichTextEditorUI extends JPanel{
    
    private JTextPane textPane;
    private JToolBar toolBar;
    private JButton bold;
    private JButton heading1;
    private JButton image;
    
    public RichTextEditorUI() {
        super(new BorderLayout());
        initComponents();
    }
    
    protected void initComponents() {
        initTextComponent();
        initControlToolBar();
        initListeners();
        initLayout();
    }

    protected void initTextComponent() {
        textPane = new JTextPane();
        textPane.setContentType("text/html; charset=utf-8");
        textPane.setText(getTestText());
    }

    protected void initControlToolBar() {
        toolBar = new JToolBar();
        bold = new JButton();
        toolBar.add(bold);
        heading1 =  new JButton();
        toolBar.add(heading1);
        image = new JButton();
        toolBar.add(image);
    }

    protected void initLayout() {
        add(toolBar, BorderLayout.NORTH);
        add(new JScrollPane(textPane), BorderLayout.CENTER);
    }

    protected void initListeners() {
        Action action = new StyledEditorKit.BoldAction();
        action.putValue(Action.NAME, "B");
        bold.setAction(action);
        action = new HeadingAction();
        action.putValue(Action.NAME, "H");
        heading1.setAction(action);
        action = new InsertImageAction(textPane);
        action.putValue(Action.NAME, "Im");
        image.setAction(action);
    }
    
    private String getTestText() {
        return "<html>This is a test plain string.<br/><em>This is a test italic string.</em></html>";
    }
}
